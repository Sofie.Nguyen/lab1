package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid1.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid1.add(new ArrayList<>(Arrays.asList(31, 32, 33)));

        removeRow(grid1, 0);
        for (int i = 0; i < grid1.size(); i++) {
            System.out.println(grid1.get(i));
        }
        // [21, 22, 23]
        // [31, 32, 33]

        ArrayList<ArrayList<Integer>> grid2 = new ArrayList<>();
        grid2.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid2.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid2.add(new ArrayList<>(Arrays.asList(31, 32, 33)));

        removeRow(grid2, 1);
        for (int i = 0; i < grid2.size(); i++) {
            System.out.println(grid2.get(i));
        }
        // [11, 12, 13]
        // [31, 32, 33]



    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
        System.out.println(grid);
    }
    
        public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
            int testSum = 0;
            for (int num : grid.get(0)) {
                testSum += num;
            }
            ;
    
            for (int rowInRows = 0; rowInRows < grid.size(); rowInRows++) {
                int rowSum = 0;
                for (int colInRows = 0; colInRows < grid.get(0).size(); colInRows++) {
                    rowSum += grid.get(rowInRows).get(colInRows);
                }
                if (rowSum != testSum) {
                    return false;
                }
            }
    
            for (int colInCols = 0; colInCols < grid.get(0).size(); colInCols++) {
                int colSum = 0;
                for (int rowInCols = 0; rowInCols < grid.size(); rowInCols++) {
                    colSum += grid.get(rowInCols).get(colInCols);
                }
                if (colSum != testSum) {
                    return false;
                }
            }
            return true;
        }
    }


 

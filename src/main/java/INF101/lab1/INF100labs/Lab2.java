package INF101.lab1.INF100labs;

import java.time.Year;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/2/
 */
public class Lab2 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("hei", "hallo", "hellooo");

        boolean leapYear1 = isLeapYear(2022);
        System.out.println(leapYear1); // false

        boolean leapYear2 = isLeapYear(1996);
        System.out.println(leapYear2); // true

        boolean leapYear3 = isLeapYear(1900);
        System.out.println(leapYear3); // false

        boolean leapYear4 = isLeapYear(2000);
        System.out.println(leapYear4); // true

        boolean evenPositive1 = isEvenPositiveInt(123456);
        System.out.println(evenPositive1); // true

        boolean evenPositive2 = isEvenPositiveInt(-2);
        System.out.println(evenPositive2); // false

        boolean evenPositive3 = isEvenPositiveInt(123);
        System.out.println(evenPositive3); // false

    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int longestWord1 = word1.length();
        int longestWord2 = word2.length();
        int longestWord3 = word3.length();

        int maxlength = Math.max((longestWord1), Math.max(longestWord2, longestWord3));

        if (maxlength == longestWord1) {
            System.out.println(word1);
        }
        if (maxlength == longestWord2) {
            System.out.println(word2);
        }
        if (maxlength == longestWord3) {
            System.out.println(word3);
        }

    }

    public static boolean isLeapYear(int year) {
        if ((year % 4) == 0) {
            if ((year % 100) == 0) {
                if ((year % 400) == 0)
                    return true;
                else
                    return false;
            } else
                return true;
        }
        return false;

    }

    public static boolean isEvenPositiveInt(int num)
    
{
        if (num > 0 && num % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

}

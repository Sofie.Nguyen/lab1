package INF101.lab1.INF100labs;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<Integer> list1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        ArrayList<Integer> removedList1 = removeThrees(list1);
        System.out.println(removedList1); // [1, 2, 4]
        
        ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(1, 2, 3, 3));
        ArrayList<Integer> removedList2 = removeThrees(list2);
        System.out.println(removedList2); // [1, 2]
        
        ArrayList<Integer> list3 = new ArrayList<>(Arrays.asList(3, 3, 1, 3, 2, 4, 3, 3, 3));
        ArrayList<Integer> removedList3 = removeThrees(list3);
        System.out.println(removedList3); // [1, 2, 4]
        
        ArrayList<Integer> list4 = new ArrayList<>(Arrays.asList(3, 3));
        ArrayList<Integer> removedList4 = removeThrees(list4);
        System.out.println(removedList4); // []
        

    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> newList = new ArrayList<>(list);
        newList.removeIf(n -> (n == 3));
        return newList;

    }

    public static List<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> newList = new ArrayList<Integer>();
        for (int i : list) {
            if (!newList.contains(i)) {
                newList.add(i);
            }
        }
        return newList;
    } 
    

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < a.size(); i++) {
            a.set(i, a.get(i) + b.get(i));
        }
    }

}
